Git 修改篇
===========

1. amend
1. revert
1. reset
1. rebase


amend
-----

修改 commit 行為錯誤
branch: 01-amend

1. 修改已經commit 內容本身的錯誤
```
1. git log
1. git commit --amend
1. git log
```


1. commit之後，發現沒有加到特定的file，但屬於同一次的commit，要把它加回去

```
1. touch page1.php
2. git commit -m 'add page1 file'
3. touch page2.php
4. git add page2.php
5. git commit --amend
6. git log
```

revert
------
1. git revert [hashid or HEAD ref]  //revert bad smell code
1. git revert conflict & fix //revert code4

適用於已經push出去的修改

reset
-----

重新織組commit的工具


|     | HEAD | Index | Working Directory |
|soft | Yes  | No    |       No          |
|mixed| Yes  | Yes   |       No          |
|hard | Yes  | Yes   |       Yes         |

1. git reset --soft HEAD^   //回去stage的狀態，準備commit的步驟,適用於不需改變commit的檔案，但要改寫commit的log
2. git reset --mixed HEAD^  //回去modified的狀態，準備add的步驟，適用於要重新結構 commit 的檔案
3. git reset --hard HEAD^   //回去上一個已經commit的狀態，適用於棄置所有變更

1. 練習三種不同的參數
2. 合併某個commit
3. 拆解某個commit



rebase
-------
rebase : 
1. 能夠以別的branch為基礎，整理自己的commit過程，將commit的base移到base branch的最後commit之上接回去(而不是當初checkout out出來的commit)。這樣可以減少一次merge，也會減少分叉。
1. 使用情境: 拉回遠端的branch時，先做整理，確保自己的branch可以乾淨地併回去
2. 整理自身branch 的commit


git rebase -i <不變動的commit的SHA-1>

參考資源待整理
```
# http://gitbook.liuhui998.com/4_2.html
# http://gitbook.liuhui998.com/4_3.html
# https://git-scm.com/book/zh-tw/v1/Git-%E5%88%86%E6%94%AF-%E5%88%86%E6%94%AF%E7%9A%84%E8%A1%8D%E5%90%88
# http://www.slideshare.net/WillHuangTW/git-merge-rebase
# https://blog.yorkxin.org/posts/2011/07/29/git-rebase/
```
